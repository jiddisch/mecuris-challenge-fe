import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';
import { ModelAttributesService } from 'src/app/core/model-attributes/model-attributes.service';
import { SeaWheelStoreService } from 'src/app/core/sea-wheel-store/sea-wheel-store.service';
import { NotificationService } from 'src/app/core/notifications/notification.service';

@Component({
  selector: 'muf-model-attributes',
  templateUrl: './model-attributes.component.html',
  styleUrls: ['./model-attributes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelAttributesComponent implements OnInit {
  public configuratorForm: FormGroup;
  public submitted = false;

  constructor(
    private notificationService: NotificationService,
    private seaWheelStoreService: SeaWheelStoreService,
    private modelAttributesService: ModelAttributesService
  ) {}

  public ngOnInit(): void {
    this.configuratorForm = new FormGroup({
      name: new FormControl('', Validators.required),
      color: new FormControl('#FF0000'),
    });
  }

  public updateColor(): void {
    this.modelAttributesService.setModelAttributes({ color: this.configuratorForm.get('color').value });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.configuratorForm.controls;
  }

  public onSubmit(): void {
    this.submitted = true;

    if (!this.configuratorForm.invalid) {
      this.seaWheelStoreService.addSeaWheel(this.configuratorForm.value).subscribe(() => {
        this.notificationService.success('Your SeaWheel has been successfully added to your SeaWheel collection');
        this.submitted = false;
        this.configuratorForm.controls.name.reset();
      });
    } else {
      console.error('Form not valid');
    }
  }
}
