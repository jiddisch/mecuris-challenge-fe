import { Component, ChangeDetectionStrategy } from '@angular/core';
import { SeaWheelStoreService } from 'src/app/core/sea-wheel-store/sea-wheel-store.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'muf-my-collection',
  templateUrl: './my-collection.component.html',
  styleUrls: ['./my-collection.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyCollectionComponent {
  public seaWheels$ = this.seaWheelStoreService.seaWheels$;

  constructor(private seaWheelStoreService: SeaWheelStoreService, private toastr: ToastrService) {}

  public deleteSeaWheel(id: string): void {
    this.seaWheelStoreService.removeSeaWheel(id).subscribe((res) => {
      this.toastr.success('Your SeaWheel has been successfully deleted from your SeaWheel collection', 'Success');
    });
  }
}
