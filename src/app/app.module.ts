import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app/app.component';
import { MyCollectionComponent } from './features/my-collection/my-collection.component';
import { ModelAttributesComponent } from './features/model-attributes/model-attributes.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './features/header/header.component';
import { ToastrModule } from 'ngx-toastr';
import { ModelComponent } from './features/model/model.component';
import { CoreModule } from './core/core.module';
import { GraphQLModule } from './graphql.module';

@NgModule({
  declarations: [AppComponent, MyCollectionComponent, ModelAttributesComponent, HeaderComponent, ModelComponent],
  imports: [BrowserModule, BrowserAnimationsModule, ToastrModule.forRoot(), CoreModule, GraphQLModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
