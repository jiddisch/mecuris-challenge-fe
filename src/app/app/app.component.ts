import { Component } from '@angular/core';

@Component({
  selector: 'muf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor() {}
}
