import { TestBed } from '@angular/core/testing';
import { SeaWheelStoreService } from './sea-wheel-store.service';

describe('ConfiguratorService', () => {
  let service: SeaWheelStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SeaWheelStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
