import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SeaWheel } from '../../shared/sea-wheel.model';
import { SeaWheelService } from '../sea-wheel/sea-wheel.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SeaWheelStoreService {
  private readonly seaWheelsSubject = new BehaviorSubject<SeaWheel[]>([]);

  constructor(private seaWheelService: SeaWheelService) {
    this.seaWheelService.index().subscribe((seaWheels) => {
      this.setSeaWheels(seaWheels);
    });
  }

  readonly seaWheels$ = this.seaWheelsSubject.asObservable();

  get seaWheels(): SeaWheel[] {
    return this.seaWheelsSubject.getValue();
  }

  private setSeaWheels(seawheels: SeaWheel[]): void {
    this.seaWheelsSubject.next(seawheels);
  }

  public addSeaWheel(seaWheel: SeaWheel): Observable<SeaWheel> {
    return this.seaWheelService.create(seaWheel).pipe(
      tap((res) => {
        this.setSeaWheels([...this.seaWheels, res]);
      })
    );
  }

  public removeSeaWheel(id: string): Observable<any> {
    return this.seaWheelService.remove({ id }).pipe(
      tap((res) => {
        this.setSeaWheels(this.seaWheels.filter((seaWheel) => seaWheel.id !== id));
      })
    );
  }
}
