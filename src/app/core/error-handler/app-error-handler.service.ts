import { Injectable, ErrorHandler } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { NotificationService } from './../notifications/notification.service';

/**
 * Application-wide error handler that adds a UI notification to the error handling
 */
@Injectable()
export class AppErrorHandler extends ErrorHandler {
  constructor(private notificationService: NotificationService) {
    super();
  }

  public handleError(error: Error | HttpErrorResponse): void {
    let displayMessage = 'An error occurred.';

    if (!environment.production) {
      displayMessage += ' See console for details.';
    }

    this.notificationService.error(displayMessage);

    super.handleError(error);
  }
}
