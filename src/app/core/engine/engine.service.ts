import * as THREE from 'three';
import { Injectable, ElementRef, OnDestroy, NgZone } from '@angular/core';
import { ModelAttributesService } from '../model-attributes/model-attributes.service';

@Injectable({ providedIn: 'root' })
export class EngineService implements OnDestroy {
  private canvas: HTMLCanvasElement;
  private renderer: THREE.WebGLRenderer;
  private camera: THREE.PerspectiveCamera;
  private scene: THREE.Scene;
  private light: THREE.DirectionalLight;
  private seaWheel: THREE.Mesh;
  private material: THREE.MeshPhongMaterial;
  private geometry: THREE.TorusGeometry;
  private frameId: number = null;
  private width = 290;
  private height = 290;
  private attributes: { [key: string]: any };

  public constructor(private ngZone: NgZone, private modelAttributesService: ModelAttributesService) {
    this.modelAttributesService.modelAttributes$.subscribe((attributes) => {
      this.attributes = attributes;
      this.material?.color.setStyle(this.attributes.color);
    });
  }

  public ngOnDestroy(): void {
    if (this.frameId != null) {
      cancelAnimationFrame(this.frameId);
    }
  }

  public createScene(canvas: ElementRef<HTMLCanvasElement>): void {
    this.canvas = canvas.nativeElement;

    // renderer
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      alpha: true,
      antialias: true,
    });
    this.renderer.setSize(this.width, this.height);

    // scene
    this.scene = new THREE.Scene();

    // camera
    this.camera = new THREE.PerspectiveCamera(75, this.width / this.height, 0.1, 1000);
    this.camera.position.z = 5;
    this.scene.add(this.camera);

    // light
    this.light = new THREE.DirectionalLight(0xffffff, 1.2);
    this.light.position.set(3, 4, 4);
    this.scene.add(this.light);

    // geometry
    this.geometry = new THREE.TorusGeometry(2, 0.6, 10, 100);
    this.material = new THREE.MeshPhongMaterial({ color: this.attributes.color });
    this.seaWheel = new THREE.Mesh(this.geometry, this.material);
    this.scene.add(this.seaWheel);
  }

  public animate(): void {
    this.ngZone.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }
    });
  }

  public render(): void {
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });

    this.seaWheel.rotation.x += 0.01;
    this.seaWheel.rotation.y += 0.01;
    this.renderer.render(this.scene, this.camera);
  }
}
