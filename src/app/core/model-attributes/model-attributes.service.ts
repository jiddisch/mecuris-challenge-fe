import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ModelAttributesService {
  private modelAttributes = new BehaviorSubject<{ [key: string]: any }>({ color: 'red' });

  get modelAttributes$(): Observable<{ [key: string]: any }> {
    return this.modelAttributes.asObservable();
  }

  setModelAttributes(attribute: { [key: string]: any }): void {
    this.modelAttributes.next(attribute);
  }

  constructor() {}
}
