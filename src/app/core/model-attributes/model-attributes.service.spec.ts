import { TestBed } from '@angular/core/testing';

import { ModelAttributesService } from './model-attributes.service';

describe('ModelAttributesService', () => {
  let service: ModelAttributesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModelAttributesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
