import { TestBed } from '@angular/core/testing';

import { SeaWheelService } from './sea-wheel.service';

describe('SeaWheelService', () => {
  let service: SeaWheelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SeaWheelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
