import { Injectable } from '@angular/core';
import { SeaWheel } from 'src/app/shared/sea-wheel.model';
import { Observable } from 'rxjs';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SeaWheelService {
  constructor(private apollo: Apollo) {}

  private query(query) {
    return this.apollo.watchQuery<any>(query);
  }

  private mutate(mutation, variables) {
    return this.apollo.mutate<any>({ mutation, variables });
  }

  public index(): Observable<SeaWheel[]> {
    return this.query({
      query: gql`
        query GetSeawheels {
          seawheels {
            id
            name
            color
          }
        }
      `,
    }).valueChanges.pipe(
      map((res) => {
        return res.data.seawheels;
      })
    );
  }

  public create(seaWeel: SeaWheel) {
    return this.mutate(
      gql`
        mutation CreateSeawheel($name: String!, $color: String!) {
          createSeawheel(input: { name: $name, color: $color }) {
            ok
            seawheel {
              id
              name
              color
            }
          }
        }
      `,
      seaWeel
    ).pipe(
      map((res) => {
        console.log(res.data.createSeawheel.seawheel);
        return res.data.createSeawheel.seawheel;
      })
    );
  }

  public remove(id: {}): Observable<any> {
    return this.mutate(
      gql`
        mutation DeleteSeawheel($id: ID!) {
          deleteSeawheel(id: $id) {
            ok
          }
        }
      `,
      id
    );
  }
}
