import { Injectable, NgZone, Inject, Injector } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(@Inject(Injector) private readonly injector: Injector) {}

  public error(message: string): void {
    this.toastrService.error(message, null, { onActivateTick: true });
  }

  public success(message: string): void {
    this.toastrService.success(message, null, { onActivateTick: true });
  }

  /**
   * Need to get ToastrService from injector rather than constructor injection to avoid cyclic dependency error
   */
  private get toastrService(): ToastrService {
    return this.injector.get(ToastrService);
  }
}
